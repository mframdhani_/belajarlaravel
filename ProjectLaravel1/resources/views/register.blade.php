<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
<h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name :</label><br><br>
        <input type="text" name="nama1"><br><br>
        <label>Last Name :</label><br><br>
        <input type="text" name="nama2"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="gdr">Male<br>
        <input type="radio" name="gdr">Female<br><br>
        <label>Nationality</label><br><br>
        <select name="negara">
            <option value="1">Indonesia</option>
            <option value="2">Amerika</option>
            <option value="2">Inggris</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>
        <label>Bio</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up"><br>
    </form>
</body>
</html>